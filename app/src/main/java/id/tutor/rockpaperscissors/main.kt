package id.tutor.rockpaperscissors


fun main() {

    var computerChoice = ""
    var playerChoice = ""

    println("Rock,Paper,Scissors? Select one")
    playerChoice = readln()
    val randomNumber = (1..3).random()
    if(randomNumber == 1){
        computerChoice = "Rock"
    }else if(randomNumber == 2){
        computerChoice = "Paper"
    }else{
        computerChoice = "Scissors"
    }

    println(computerChoice)

    val winner = when{
        playerChoice == computerChoice -> "Tie"
        playerChoice == "Rock" && computerChoice == "Scissors" -> "Player Win"
        playerChoice == "Paper" && computerChoice == "Rock" -> "Player Win"
        playerChoice == "Scissors" && computerChoice == "Paper" -> "Player Win"
        else -> "Computer Win"
    }

    println(winner)
}